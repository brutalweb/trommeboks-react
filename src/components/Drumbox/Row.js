import React, { PropTypes, Component } from 'react';
import Toggler from "./Toggler";


class Row extends Component {
  togglers = new Array(this.props.columns);
  render() {
    for(let i = 0; i<this.togglers.length; i++) {
      this.togglers[i] = false;
    }
    return (
      <tr><td>{this.props.instrument}</td>{this.togglers.map(toggler => <Toggler toggled={toggler} />)}</tr>
    );
  }

}

export default Row;
