/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './Drumbox.css';
import withStyles from '../../decorators/withStyles';
import Row from "./Row";

@withStyles(styles)
class Drumbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bpm : props.bpm
    }
  }

  handleChange = function() {
    this.setState({value: event.target.value});
  }

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  instruments = ['cl_hihat', 'claves', 'conga1', 'cowbell', 'crashcym', 'handclap', 'hi_conga', 'hightom', 'kick1', 'kick2', 'maracas', 'open_hh', 'rimshot', 'snare', 'tom1'];

  render() {
    const title = 'Drumbox';
    this.context.onSetTitle(title);
    return (
      <div className="Drumbox">
        <div className="Drumbox-container">
        <h1>{title}</h1>
        <table><tbody>{
          this.instruments.map((item => <Row instrument={item} columns={this.props.columns}/>))
        }
          </tbody></table>
          <label>BPM:</label><input type="number" value={this.state.bpm} onChange="this.handleChange"/>
          <button>LISTEN</button>
          <button>STOP</button>
        </div>
      </div>
    );
  }

}

export default Drumbox;
